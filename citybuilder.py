# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 16:47:41 2018

@author: enovi
"""
#License: GPL 3.0

#City management simulation

import random

cash                = 10000
days                = 0
small_house         = 0
medium_house        = 0
large_house         = 0
food_truck          = 0
food_truck_multiple = 1
liquor_store        = 0
bar                 = 0
gas_station         = 0
alcohol_multiple    = 1
gas_multiple        = 1
crime_multiple      = 1
fire_station        = 0
police_station      = 0
hospital            = 0
structures          = 0
residents           = 0

def main():
    choice = ''
    print('***Welcome to City Builder!***')
    print('Build homes and businesses, attract residents, prevent disasters.')
    choice = input('Start a new game y/n? ')
    if choice == 'y':
        defaults()
        city_builder()
    if choice == 'n':
        return
    
def defaults():
    global cash
    global days
    global small_house
    global medium_house
    global large_house
    global food_truck
    global food_truck_multiple
    global liquor_store
    global bar
    global gas_station
    global alcohol_multiple
    global gas_multiple
    global crime_multiple
    global fire_station
    global police_station
    global hospital
    global structures
    global residents
    cash                = 10000
    days                = 0
    small_house         = 0
    medium_house        = 0
    large_house         = 0
    food_truck          = 0
    food_truck_multiple = 1
    liquor_store        = 0
    bar                 = 0
    gas_station         = 0
    alcohol_multiple    = 1
    gas_multiple        = 1
    crime_multiple      = 1
    fire_station        = 0
    police_station      = 0
    hospital            = 0
    structures          = 0
    residents           = 0    
    
def city_status():
    global cash
    global days
    global small_house
    global medium_house
    global large_house
    global food_truck
    global liquor_store
    global bar
    global gas_station
    global fire_station
    global police_station
    global hospital
    global structures
    global residents
    choice = ''
    print('You have ${}.'.format(cash))
    print('You have {} small houses.'.format(small_house))
    print('You have {} medium houses.'.format(medium_house))
    print('You have {} large houses.'.format(large_house))
    print('You have {} food trucks.'.format(food_truck))
    print('You have {} liquor stores.'.format(liquor_store))
    print('You have {} bars.'.format(bar))
    print('You have {} gas stations.'.format(gas_station))
    print('You have {} fire stations.'.format(fire_station))
    print('You have {} police stations.'.format(police_station))
    print('You have {} hospitals.'.format(hospital))
    print('Your city has {} structures.'.format(structures))
    print('Your city has {} residents.'.format(residents))
    choice = input('Show awards y/n? ')
    if choice == 'y':
        awards()    

def build_residential():
    global cash
    global small_house
    global medium_house
    global large_house
    global structures
    choice = ''
    print('Small house costs $3500, medium house costs $7000, large house costs $14000')
    print('Small house holds 10 residents, medium holds 25, large holds 50')
    choice = input('Build (s)mall, (m)edium, or (l)arge house, or (e)xit: ').lower()
    if choice == 's':
        cash -= 3500
        small_house += 1
        structures += 1
        print('You built a small house.')
    if choice == 'm':
        cash -= 7000
        medium_house += 1
        structures += 1
        print('You built a medium house.')
    if choice == 'l':
        cash -= 14000
        large_house += 1
        structures += 1
        print('You built a large house.')
        
def build_commercial():
    global cash
    global food_truck
    global structures
    global food_truck_multiple
    global liquor_store
    global bar
    global gas_station
    global alcohol_multiple
    global gas_multiple
    global crime_multiple
    choice = '' 
    food_value = 0
    alcohol_value = 0
    gas_value = 0    
    food_value = int(300 * food_truck_multiple)
    alcohol_value = int(500 * alcohol_multiple)
    gas_value = int(750 * gas_multiple)
    print('Food truck costs $3000, Liquor store costs $5000, Bar costs $10000, Gas station costs $12000.')
    print('Workers needed: 5 for food truck, 5 for liquor store, 10 for bar, 10 for gas station.')
    print('Food truck earns ${}/day.'.format(food_value))
    print('Liquor store earns ${}/day.'.format(alcohol_value))
    print('Bar earns ${}/day.'.format(2 * alcohol_value))
    print('Gas station earns ${}/day.'.format(gas_value))
    choice = input('Build (f)ood truck, (l)iquor store, (b)ar, (g)as station, or (e)xit: ').lower()
    if choice == 'f':
        cash -= 3000
        food_truck += 1
        structures += 1
        print('You built a food truck.')
    elif choice == 'l':
        cash -= 5000
        liquor_store += 1
        crime_multiple *= 1.05
        structures += 1
        print('You built a liquor store.')
    elif choice == 'b':
        cash -= 10000
        bar += 1
        crime_multiple *= 1.03
        structures += 1
        print('You built a bar.')
    elif choice == 'g':
        cash -= 12000
        gas_station += 1
        structures += 1
        print('You built a gas station.')
    
def build_municipal():
    global cash
    global fire_station
    global police_station
    global hospital
    global crime_multiple
    global structures
    choice = ''
    police_coverage = 0
    police_coverage = int(20 / crime_multiple)    
    print('Fire stations, police stations, and hospitals each cost $20,000.')
    print('Operating costs for each station are $400 per day.')
    print('Each fire station currently protects 20 structures.')
    print('Each police station currently protects {} structures.'.format(police_coverage))
    print('Each hospital currently protects 20 structures.')
    choice = input('Build (f)ire station, (p)olice station, (h)ospital, or (s)kip? ')
    if choice == 'f':
        cash -= 20000
        fire_station += 1
        structures += 1
        print('You built a fire station.')
    if choice == 'p':
        cash -= 20000
        police_station += 1
        structures += 1
        print('You built a police station.')
    if choice == 'h':
        cash -= 20000
        hospital += 1
        structures += 1
        print('You built a hospital.')
        
def capacity():
    global small_house
    global medium_house
    global large_house
    global residents
    capacity = 0
    res_change = 0
    capacity += small_house * 10
    capacity += medium_house * 25
    capacity += large_house * 50 
    if residents <= capacity:
        res_change = int(capacity * 0.1)
        print('Your city gained {} residents.'.format(res_change))
        residents += res_change
    elif residents > capacity:
        res_change = int((residents - capacity) * 0.2)
        if res_change > 0:
            print('Your city lost {} residents. Build more homes.'.format(res_change))
            residents -= res_change
        
def income():
    global cash
    global residents
    global food_truck
    global food_truck_multiple
    global alcohol_multiple
    global gas_multiple
    global liquor_store
    global bar
    global gas_station
    needed = 0
    earnings = 0
    #food truck revenue
    needed = food_truck * 5
    if residents >= needed:
        earnings += 300 * food_truck * food_truck_multiple
    else:
        earnings += 300 * food_truck * (residents/needed) * food_truck_multiple
    #gas station revenue
    needed += liquor_store * 5
    if residents >= needed:
        earnings += 500 * liquor_store * alcohol_multiple
    else:
        earnings += 500 * liquor_store * (residents/needed) * alcohol_multiple
    #bar revenue
    needed += bar * 10
    if residents >= needed:
        earnings += 1000 * bar * alcohol_multiple
    else:
        earnings += 1000 * bar * (residents/needed) * alcohol_multiple
    #gas station revenue
    needed += gas_station * 10
    if residents >= needed:
        earnings += 750 * gas_station * gas_multiple
    else:
        earnings += 750 * gas_station * (residents/needed) * gas_multiple
    earnings = int(earnings)  
    print('Your city earned ${}.'.format(earnings))    
    cash += earnings
    
def interest():
    global cash
    interest_payment = 0
    # 6% interest on municipal bonds = -0.06
    interest_rate = -0.06
    if cash < 0:
        interest_payment = int(cash * interest_rate)
        cash -= interest_payment
        print('You paid ${} to bond holders.'.format(interest_payment))
        #high debt load warning
        if cash < -20000:
            print('Your city will go bankrupt if its debt rises above $50,000.')
            
def expenses():            
    global cash
    global fire_station
    global police_station
    global hospital     
    fire_cost     = 0
    police_cost   = 0
    hospital_cost = 0
    expenses      = 0
    fire_cost     = fire_station * 400
    police_cost   = police_station * 400
    hospital_cost = hospital * 400
    expenses      = fire_cost + police_cost + hospital_cost
    #pay expenses
    cash -= fire_cost
    cash -= police_cost
    cash -= hospital_cost
    print('You paid ${} for municipal services.'.format(expenses))    
    
def daily_event():
    global cash
    global fire_station
    global food_truck_multiple
    global alcohol_multiple
    global gas_multiple
    global crime_multiple
    global residents
    random_event = 0
    random_cash = 0
    resident_factor = 0
    resident_factor = int(1 + residents/200)
    random_event = random.randint(0,30)
    if random_event == 0:
        if residents >= 100:
            print('The citizens throw a parade in your honor.')
        else:
            print('Your citizens grill burgers in an empty lot.')
    if random_event == 1:
        print('The food trucks introduce a new entree and sales rise.')
        food_truck_multiple *= 1.12
    if random_event == 2:
        print('A citizen gets sick after eating at a food truck. Sales fall.')
        food_truck_multiple *= 0.9
        health_event()
    if random_event == 3:
        if fire_station >= 1:
            print('Your fire department rescues a cat from a tree.')
        else:
            print('A cat is stuck in a tree and howls all night.')
            residents -= random.randint(1,4) * resident_factor
    if random_event == 4:
        print('Local bands put on a concert.')
        random_cash = 100 * random.randint(1,3) * resident_factor
        print('Restaurants report higher sales. Tax receipts rise by ${}.'.format(random_cash))
        food_truck_multiple *= 1.02
        cash += random_cash        
    if random_event == 5:
        print('Housing prices fall in a neighboring city, and some citizens move away.')
        residents -= random.randint(1,5)
    if random_event == 6:    
        print('Heavy rainfall causes nearby rivers to overflow.')
        random_cash = 100 * random.randint(1,5) * resident_factor
        print('You pay ${} to repair flood damage.'.format(random_cash))
        cash -= random_cash
    if random_event == 7:
        print('Your city wins a grant from the state government.')
        random_cash = 100 * random.randint(1,20) * resident_factor
        print('The grant was worth ${}.'.format(random_cash))
        cash += random_cash
    if random_event == 8:    
        print('An audit finds accounting discrepancies in the city budget.')
        random_cash = 100 * random.randint(1,8) * resident_factor
        print('The books were off by ${}.'.format(random_cash))
        cash -= random_cash
    if random_event == 9:    
        print('The sky is clear today.')
    if random_event == 10:
        print('The county fair takes place.')
        random_cash = 100 * random.randint(1,8) * resident_factor
        print('You collect ${} in tax revenue from the fair.'.format(random_cash))
        cash += random_cash
    if random_event == 11:
        print('A fire breaks out in a local building.')
        fire_event()
    if random_event == 12:
        print('A resident reports a crime in progress.')
        police_event()
    if random_event == 13: 
        if residents >= 200:
            print('A local artist paints a portrait of the mayor.')
        else:
            print('A local artist makes some chalk drawings on the sidewalk.')
    if random_event == 14:
        print('A flock of pigeons nests in some buildings downtown.')
    if random_event == 15:
        print('It is a foggy day today.')
    if random_event == 16:
        print('A flu outbreak occurs in your city.')
        health_event()
    if random_event == 17:
        print('Your police detectives discover something suspicious while patrolling.')
        police_event()
    if random_event == 18:
        print('A large wildfire approaches the city.')
        fire_event()
        print('The wildfire ignites another fire in your city.')
        fire_event()
        print('The wildfire has been extinguished.')
    if random_event == 19:
        print('Your city controller discovers additional funds in an overlooked account.')
        cash += 500 * random.randint(1,10) * resident_factor
    if random_event == 20:
        print('The county fair hosts a popular art show.')
        cash += 100 * random.randint(1,5) * resident_factor
    if random_event == 21:
        print('War breaks out in the Middle East.')
        gas_multiple *= 1.06
    if random_event == 22:
        print('A new oil field is discovered.')
        gas_multiple *= 0.95
    if random_event == 23:
        print('The state government raises excise taxes on alcohol.')
        alcohol_multiple *= 0.9
    if random_event == 24:
        print('A major brewery expands production capacity.')
        alcohol_multiple *= 1.12
    if random_event == 25:
        print('Citizens start a neighborhood watch program.')
        crime_multiple *= 0.97
    if random_event == 26:
        print('A restaurant cook prepares food with unwashed hands.')
        food_truck_multiple *= 0.95
        health_event()
    if random_event == 27:
        print('An escaped convict moves into your town.')
        police_event()
    if random_event == 28:
        print('Cooking spice manufacturers lower their prices.')
        food_truck_multiple *= 1.02
    if random_event == 29:
        print('Brunch becomes more popular.')
        food_truck_multiple *= 1.02
        alcohol_multiple *= 1.02
    if random_event == 30:    
        print('A fire breaks out at a local campground.')
        fire_event()
        
def fire_event():
    global residents
    global structures
    global fire_station
    resident_factor = 0
    resident_factor = int(1 + residents/200)
    frequency       = 0
    coverage        = 0
    needed          = 0
    coverage        = 20 + (fire_station * 20)
    needed          = structures
    if coverage >= needed:
        print('Your fire trucks quickly put out the fire.')
        frequency = 1
        burn(frequency)
    elif coverage >= needed / 2:
        print('The fire trucks eventually put out the fire.')
        frequency = 2 * resident_factor
        burn(frequency)
    elif coverage >= needed / 3:
        print('The fire trucks struggle to put out the fire.')
        frequency = 3 * resident_factor
        burn(frequency)    
    else:
        print('The fire grows, and burns down a large section of town.')
        frequency = 4 * resident_factor
        burn(frequency)
        
def burn(frequency):
    global small_house
    global medium_house
    global large_house
    global food_truck
    global liquor_store
    global bar
    global gas_station
    global police_station
    global fire_station
    global hospital
    global structures
    target = 0
    i = 0
    while i < frequency:
        target = random.randint(0,9)
        if target == 0:
            small_house    -= 1
        elif target == 1:
            medium_house   -= 1
        elif target == 2:
            large_house    -= 1
        elif target == 3:
            food_truck     -= 1
        elif target == 4:
            liquor_store   -= 1
        elif target == 5:
            bar            -= 1
        elif target == 6:
            gas_station    -= 1
        elif target == 7:
            police_station -= 1
        elif target == 8:
            fire_station   -= 1
        elif target == 9:
            hospital       -= 1    
        structures         -= 1
        i += 1
        
def police_event():
    global cash
    global police_station
    global crime_multiple
    global residents
    global structures
    coverage = 0
    needed = 0
    loss = 0
    cash_loss = 0
    resident_loss = 0
    resident_factor = 0
    resident_factor = int(1 + residents/200)
    coverage = (20 + (police_station * 20)) / crime_multiple
    needed   = structures
    if coverage >= needed:
        print('The police quickly apprehend the criminal.')
        residents -= 1
    elif coverage >= needed / 2:
        print('The police apprehend the criminal after a few more crimes occur.')
        loss = random.randint(1,5)
        resident_loss = loss * resident_factor
        residents -= resident_loss
        cash_loss = loss * 500 * resident_factor
        cash -= cash_loss
        print('You lost ${} and {} residents.'.format(cash_loss, resident_loss))
    else:
        print('The criminal goes on a crime spree.')
        loss = random.randint(1,8)
        resident_loss = loss * 2 * resident_factor
        residents -= resident_loss
        cash_loss = loss * 1000 * resident_factor
        cash -= cash_loss
        print('You lost ${} and {} residents.'.format(cash_loss, resident_loss))
        
def health_event():
    global cash
    global hospital
    global residents
    global structures
    coverage = 0
    needed = 0
    resident_loss = 0
    resident_factor = 0
    resident_factor = int(1 + residents/200)
    coverage = 20 + (hospital * 20)
    needed = structures
    if coverage >= needed:
        print('Your hospitals successfully treated your sick citizens.')
    elif coverage >= needed / 2:
        print('Many residents recovered, but a few of them died.')
        resident_loss = resident_factor * random.randint(1,10)
        residents -= resident_loss
        print('You lost {} residents.'.format(resident_loss))
    else:    
        print('Your city residents are sickened by disease.')
        resident_loss = resident_factor * random.randint(1,50)
        residents -= resident_loss
        print('You lost {} residents.'.format(resident_loss))
        
def reset_negatives():
    global residents
    global small_house
    global medium_house
    global large_house
    global food_truck
    global liquor_store
    global bar
    global gas_station
    global fire_station
    global police_station
    global hospital
    global structures
    if residents < 0:
        residents = 0
    #prevent structure counts from going negative
    if small_house < 0:
        structures -= small_house
        small_house = 0
    if medium_house < 0:
        structures -= medium_house
        medium_house = 0
    if large_house < 0:
        structures -= large_house
        large_house = 0
    if food_truck < 0:
        structures -= food_truck
        food_truck = 0
    if liquor_store < 0:
        structures -= liquor_store
        liquor_store = 0
    if bar < 0:
        structures -= bar
        bar = 0
    if gas_station < 0:
        structures -= gas_station
        gas_station = 0
    if fire_station < 0:
        structures -= fire_station
        fire_station = 0
    if police_station < 0:
        structures -= police_station
        police_station = 0
    if hospital < 0:
        structures -= hospital
        hospital = 0 

def awards():
    global residents
    award_list = []
    if residents >= 300:
        award_list.append('statue')
    if residents >= 600:
        award_list.append('community center')
    if residents >= 1000:
        award_list.append('city_hall')
    if residents >= 3000:
        award_list.append('central plaza')       
    if residents >= 6000:
        award_list.append('mansion')
    if residents >= 10000:
        award_list.append('conference center')
    if residents >= 30000:
        award_list.append('state park')
    if residents >= 60000:
        award_list.append('county seat')
    #only show awards if at least one award has been granted    
    if residents >= 300:
        print(award_list)
    else:
        print('No awards earned yet.')
    
def city_builder():
    global cash
    global days
    gameloop = True
    choice = ''
    while gameloop == True:
        #day counter
        days += 1
        print('It is day {}.'.format(days))
        #quit or continue
        choice = input('Continue the game? y to continue, q to quit: ').lower()
        if choice == 'y':
            gameloop = True
        if choice == 'q':
            gameloop = False
            return
        #check inventory
        choice = input('Check city status? y/n: ')
        if choice == 'y':
            city_status()
        #decision phase
        choice = input('Build (r)esidential, (c)ommercial, or (m)unicipal structures, or (s)kip? ').lower()
        if choice == 'r':
            build_residential()
        if choice == 'c':
            build_commercial()
        if choice == 'm':
            build_municipal()
        if choice == 's':
            print('You decide not to build anything today.')
        #population growth        
        capacity()
        #income
        income()
        #interest
        interest()
        #expenses
        expenses()
        #daily event
        daily_event()
        #no negative values        
        reset_negatives()
        #loss condition
        if cash < -50000:
            print('Creditors refuse to buy your bonds, and your city declares bankruptcy.')
            gameloop = False
main()